FROM golang:latest
#https://github.com/gianebao/bitbucket-pipeline-go-mysql/blob/master/Dockerfile

ENV MYSQL_MAJOR 5.7
#https://packages.debian.org/search?keywords=mysql-server
ENV MYSQL_ROOT_PASSWORD root

# Update and Fix Language
RUN \
 apt-get update && apt-get -y upgrade &&\
 apt-get -y --no-install-recommends install locales &&\
 echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen &&\
 locale-gen en_US.UTF-8 &&\
 /usr/sbin/update-locale LANG=en_US.UTF-8

# Add MySQL repository
RUN \
 apt-key adv --keyserver ha.pool.sks-keyservers.net --recv-keys A4A9406876FCBD3C456770C88C718D3B5072E1F5 &&\
 echo "deb http://repo.mysql.com/apt/debian/ jessie mysql-$MYSQL_MAJOR" > /etc/apt/sources.list.d/mysql.list &&\
 apt-get update &&\
 { \
       echo mysql-community-server mysql-community-server/data-dir select ''; \
       echo mysql-community-server mysql-community-server/root-pass password $MYSQL_ROOT_PASSWORD; \
       echo mysql-community-server mysql-community-server/re-root-pass password $MYSQL_ROOT_PASSWORD; \
       echo mysql-community-server mysql-community-server/remove-test-db select false; \
   } | debconf-set-selections &&\
 apt-get -y --no-install-recommends install mysql-server

# Install built in's
RUN \
 apt-get -y --no-install-recommends install mysql-client redis-server &&\
 apt-get autoclean && apt-get clean && apt-get autoremove

#Go Packete
RUN go get github.com/denisenkom/go-mssqldb 
RUN go get github.com/go-sql-driver/mysql
RUN go get github.com/jinzhu/gorm 
RUN go get github.com/lib/pq
RUN go get github.com/PuerkitoBio/goquery
